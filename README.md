Project
========================================

About
-----

Project contain websockets apps.

Prerequisites
-------------

- Python >= 3
- pip
- virtualenv (virtualenvwrapper is recommended)
- Mysql

Installation
------------

Ubuntu 16.04

Packages
```
$ sudo apt-get install python-pip #optional
$ sudo apt-get install libmysqlclient-dev
# Usage Ubuntu
$ sudo apt install python3-dev libpython3-dev
$ sudo apt-get install redis-server
$ sudo apt-get install supervisor
$ sudo apt install virtualenv

```

Set locales

```
$ export LC_ALL="en_US.UTF-8"
$ export LC_CTYPE="en_US.UTF-8"
$ sudo dpkg-reconfigure locales
```

Redis
```
# Test server redis
$ redis-cli ping
```

Environment

```
# Install virtualenv
$ virtualenv -p python3 venv

# Activate virturalenv
$ source venv/bin/activate
```

Install requeriments

```
$ pip install -r requirements.txt
```

Create config file
```
$ cp settings.example.json settings.json
# Change values in your config
```

Run migrations

```
$ python manage.py migrate
```

Run collectstatic

```
$ python manage.py collectstatic
```

Run local
```
$ python manage.py runserver
```

Run production
```
# Setup supervisor config folder /nginx
```


* * *

Documentation
------------

* [Python](https://www.python.org/doc/)
* [Django](https://docs.djangoproject.com/en/2.0/)
* [Pip, Virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
* [Redis](https://redis.io/topics/quickstart)
