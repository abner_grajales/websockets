from django.conf.urls import url

from . import consumers

url_patterns = [

    url(r'^ws/chat/(?P<room_name>.*)/$', consumers.ChatConsumer),
]
