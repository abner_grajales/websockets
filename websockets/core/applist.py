# Application definition

__BEFORE_DJANGO_APPS = (
)

__DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'channels',
)

__OWN_APPS = (
    'websockets.apps.chat',
)

__THIRD_PARTY_APPS = (
    'rest_framework',
    'corsheaders',
)

INSTALLED_APPS = __BEFORE_DJANGO_APPS + __DJANGO_APPS + __OWN_APPS
INSTALLED_APPS = INSTALLED_APPS + __THIRD_PARTY_APPS
