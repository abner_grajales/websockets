from ..core.json_settings import get_settings

settings = get_settings()

EMAIL_USE_TLS = settings["EMAIL"]["EMAIL_USE_TLS"]
DEFAULT_FROM_EMAIL = settings["EMAIL"]["DEFAULT_FROM_EMAIL"]
EMAIL_BACKEND = settings["EMAIL"]["EMAIL_BACKEND"]
SPARKPOST_API_KEY = settings["EMAIL"]["SPARKPOST_API_KEY"]

SPARKPOST_OPTIONS = {
    'track_opens': False,
    'track_clicks': False,
    'transactional': True,
}
