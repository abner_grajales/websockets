from ..core.json_settings import get_settings

settings = get_settings()

DATABASES = settings['DB']
